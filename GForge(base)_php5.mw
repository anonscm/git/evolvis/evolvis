== WARNING ==

This is historical information. Up-to-date information for the Evolvis 5 generation can be found at the [[Evolvis_installation|installation]] page or its [[Evolvis_installieren|german version]].

Proceed below at your own risk. This is the PHP5-based EvolvisForge 4.5 (even pre-4.8, so dated about 2008).

== Preparations ==
The GForge base system will be installed on a ''debian etch'' linux. Before you can start installing the GForge packages install ''postgresql,'' the ''apache2'' webserver with ''php5,'' ''subversion,'' ''proftpd,'' ''postfix'' and ''mailman.''
You need the following debian packages:
 postgresql-8.1 apache2 libapache2-mod-php5 php5-pgsql subversion proftpd postfix mailman

== GForge installation ==
The evolvis GForge code can be found in the [http://evolvis.org/plugins/scmsvn/viewcvs.php/trunk/gforge_base/gforge/?root=evolvis SCM Repository]).

==== Building the debian packages ====
The debian packages have to be built from the files of the SCM. 
You must install the following debian packages to create your own packages:
 binutils gcc libc6-dev libg++ make fakeroot cpp cpio file debmake debhelper dpkg-dev devscripts patch debian-policy developers-reference dpatch sharutils docbook-to-man lockfile-progs libdbi-perl libdbd-pg-perl ibhtml-parser-perl libtext-autoformat-perl php5-cli php5-cgi   
First check out the files:
Without LDAP:
 svn checkout svn://svn.evolvis.org/svnroot/evolvis/branches/php-v5-noldap-branch/gforge
With LDAP (see [[Ldap]]):
 svn checkout svn://svn.evolvis.org/svnroot/evolvis/trunk/gforge_base/gforge
The code for the svn plugin is here:
 svn checkout svn://svn.evolvis.org/svnroot/evolvis/trunk/gforge_base/gforge_plugins/gforge-plugin-scmsvn
Then build the debian packages in the checked out directories. The ''debian/rules'' file controles the building of the packages. Enter the ''gforge'' and afterwards the ''gforge-plugin-scmsvn'' folder and start the build process with:
 dpkg-buildpackage
The created packages will be saved in the parent folder.

==== Installing the debian packages====
Be aware that the debian packages will configure your GForge system automatically. The entries of the /etc/hosts file will be used to set the proper IP adresses and hostnames to the GForge config files. Be sure that there is listed your right IP adress (like 123.12.123.12 gforge.domain.abc gforge). Changing this values afterwards will be much more work.
Install the the created GForge packages with dpkg.
 dpkg -i gforge_<version>.deb ...

===== gforge-common =====
 dpkg -i gforge-common_4.5.14-22etch2.3_all.deb
 dpkg-reconfigure gforge-common
to set the Name of the evolvis-system.

===== gforge-db-postgresql =====
 aptitude install libdbi-perl libdbd-pg-perl libhtml-parser-perl libtext-autoformat-perl php5-cli php5-cgi
 dpkg -i gforge-db-postgresql_4.5.14-22etch2.3_all.deb
 dpkg-reconfigure gforge-db-postgresql
to check the settings, especially the IP-config.

===== gforge-mta-postfix =====
 aptitude install postfix-pgsql
 dpkg -i gforge-mta-postfix_4.5.14-22etch2.3_all.deb

[[GForge(base)_php5#Database (postgresql)|Configure postgresql]] to allow connection for the following packages.

===== gforge-lists-mailman =====
  dpkg -i gforge-lists-mailman_4.5.14-22etch2.3_all.deb

You have to create a new list
 newlist mailman
and add the showed Aliases to
 vim /etc/aliases
 newaliases

===== gforge-web-apache =====
 aptitude install php5-gd perl-suid cronolog
 dpkg -i gforge-web-apache_4.5.14-22etch2.3_all.deb
The ''gforge-web-apache'' package will configure the ''/etc/apache2/conf.d/gforge.httpd.conf'' file. Most of the settings will fit GForges needs.
Insert your SSL Certs in apache and restart apache2. [[GForge(base)_php5#Certificate for ssl connections|Described here]].

===== gforge-plugin-scmsvn =====
 aptitude install subversion-tools viewcvs python-subversion mksh
 dpkg -i gforge-plugin-scmsvn_4.5.14-5.1_all.deb

===== gforge-shell-postgresql =====
 aptitude install ssh libnss-pgsql1 libpam-pgsql nscd
 dpkg -i gforge-shell-postgresql_4.5.14-22etch2.3_all.deb
 dpkg-reconfigure gforge-plugin-scmsvn
 dpkg-reconfigure gforge-shell-postgresql

==== More detailed configuration ====
For more detailed installation instructions take a look at the [http://gforge.org/docman/index.php?group_id=1 GForge doku]

== Database (postgresql) ==
Configure your postgresql DBMS to listen on port 5432 and the correct IP adress. Edit your ''postgresql.conf'' file and add the correct values, e.g.:
 listen_addresses = 'localhost, 172.0.1.1, 212.79.161.114'
 port = 5432
Then you have to allow local access for the gforge user to the gforge database. Open the ''pg.hba.conf'' and add the following line:
 host    gforge      gforge        172.0.0.1/32      md5
 host    gforge      gforge        172.0.1.1/32      md5
After configuring restart the postgresql server.

==== Certificate for ssl connections  ====
Here you can find a [http://www.debian-administration.org/articles/284 Howto] that explains how to create certificates for apache.
Before you start, install the required packages:
 openssl
Then create a Certificate for ssl connections:
 make-ssl-cert /usr/share/ssl-cert/ssleay.cnf /etc/apache2/ssl/apache.pem
Afterwards edit the ''gforge.httpd.conf'' file and add the path to your certificate in all ''mod_ssl'' sections.
E.g.:
 <IfModule mod_ssl.c>
    SSLEngine on
    SSLCertificateFile /etc/apache2/ssl/apache.pem
    SSLCertificateKeyFile /etc/apache2/ssl/apache.pem
 </IfModule>

Be sure in ''/etc/apache2/ports.conf'' you listen on port 80 and 443 for SSL:
 Listen 80
 Listen 443

==== Virtual host configuration ====
There are serveral virtual hosts preconfigured:
*HTTP
**<nowiki><your hostname></nowiki>
**Alias: <nowiki>www.<your hostname></nowiki>
**Port 80, SSL: Port 443
*SCM host
**local: /usr/share/gforge/scm
*SCM HTTP vhost
**scm.<nowiki><your hostname></nowiki>
**Port 80, SSL: Port 443
*Download host
**download.<nowiki><your hostname></nowiki>
**Port 80
*List host
**lists.<nowiki><your hostname></nowiki>
**Port 80, SSL: Port 443

Adjust them to your needs.

==== PHP ====
For the GForge base system php version 4 is mandatory.
The following directives are required by GForge:
 register_globals = On
 magic_quotes_gpc = On
 file_uploads = On
 include_path=".:/var/www/gforge:/var/www/gforge/www/include:/etc/gforge"
They shoud be set automatically by the debian packages in the ''gforge.httpd.conf'' file. Alternatively you can set them in the ''php.ini''.

== Mailserver (postfix) ==
The installation of ''postfix'' with apt will remove ''exim'' and all related packages on a standard debian system. 
GForge uses the pgsql dictionary to access the postgresql Database. This dictionary is provided by the ''postfix-pgsql'' package. 
The ''/etc/postfix/dynamicmaps.cf'' tells postfix which dictionarys are available. For the pgsql dictionary the should be a line like this:
 #type   location of .so file                    open function   (mkmap func)
 #====   ================================        =============   ============
 pgsql   /usr/lib/postfix/dict_pgsql.so          dict_pgsql_open
In the ''/etc/postfix/main.cf'' configuration file all required parameters are defined to access the database. The ''gforge-mta-postfix'' package inserts the parameters for ''pgsql_gforge_lists'':
 pgsql_gforge_lists_hosts = 127.0.0.1
 pgsql_gforge_lists_user = gforge_mta
 pgsql_gforge_lists_password =
 pgsql_gforge_lists_dbname = gforge
 pgsql_gforge_lists_domain = lists.<your_hostname>
 pgsql_gforge_lists_select_field = post_address
 pgsql_gforge_lists_table = mta_lists
 pgsql_gforge_lists_where_field = list_name
The corresponding parameters for ''pgsql_gforge_users'' should be added as well.<br>
Also configure the subdomains ''lists'', ''gforge'' and ''users'' as local destinations in the ''main.cf'' file.
 mydestination = users.<your_hostname>, lists.<your_hostname>, gforge.<your_hostname>

== Mailing list (mailman) ==

== Login Management ==
All users of the GForge system are saved in the postgresql database. User which are members of a project must be able to check file into the subversion repository via svn+ssh. To get ssh access the GForge users must be mapped to OS users. This is done with ''nsswitch'' and pam. 
Read the [http://www.tuxfutter.de/wiki/Benutzerverwaltung_mit_Postgres user management with postgresql] (in german) howto to get more information.
<br>To use svn+ssh you need to install the package ''cvs'' or create a file in ''/etc/pam.d/cvs'', which content:
 @include common-auth
 @include common-account

==== svn via ssh ====
The ''anonsvnsh'' shell can be used to restrict the the system accounts to svn use only. 
''anonsvnsh'' is a modified ''anoncvs'' for subversion.
Checkout the required files from the repository:
 svn checkout svn://svn.evolvis.org/svnroot/evolvis/trunk/anonsvnsh
To compile the shell properly ''pmake'' is required. After installing ''pmake'', a simple execution of ''pmake'' in the ''anonsvnsh'' directory will compile the shell. Afterwards install it to ''/lib/anonsvnsh''.


Maybe you have to correct the rights for the file
 chmod u+s /lib/anonsvnsh
else you get the error-message: ''chroot: Operation not permitted''.


The local GForge Admin can define different shells for each user. Choose the ''/bin/bash'' or a equivalent shell to grant full ssh access or ''/lib/anonsvnsh'' for svn only access.
You have to add it to the ''/etc/shells'' file to use the shell. Gforge makes these shells listed in ''/etc/shells'' accessable for the users in the GForge settings.

==== svn via webdav ====
''WebDAV'' can be used to access the svn contents. The write mechanism of ''WebDAV'' is the interesting part, because a read-only-access could be manged by a simple apache site.
The ''WebDAV'' configuration in apache is a little tricky, especially when you want german umlauts aso. to be viewed correctly from Windows, Linux and the filesystem.

On Ubuntu Hardy this works right out of the Box with the following HowTo (Debian and others often have still some umlaut-Issues).

In apache2 the needed ''dav_fs''-, the ''dav''-mod and the ''headers''-mod is installed by default on "Debian flavoured"-systems.
We also need the ''encoding''-mod:
 apt-get install libapache2-mod-encoding

Now we need to add some lines to the apache2.conf. This can be done on the end right before the two "includes".
 BrowserMatch "Microsoft Data Access Internet Publishing Provider" redirect-carefully
 BrowserMatch "^WebDrive" redirect-carefully
 BrowserMatch "^gnome-vfs" redirect-carefully
 BrowserMatch "^WebDAVFS/1.[012]" redirect-carefully
 BrowserMatch "Microsoft-WebDAV-MiniRedir/5.1.2600" redirect-carefully
 BrowserMatch "^WebDAVFS" redirect-carefully
 
 <IfModule mod_encoding.c>
    EncodingEngine on
    NormalizeUsername on
 </IfModule>
 
 <IfModule mod_headers.c>
    Header add MS-Author-Via "DAV"
 </IfModule>

Now enable the needed apache-mods
 a2enmod dav_fs 
 a2enmod headers
 a2enmod encoding

Next step is to configure a Location directive in the gforge-httpd.conf
The following is 
 <Location /svnroot/>
     DAV svn
     # Directory with all SVN-projects
     SVNParentPath /svnroot/
     # Show all SVN projects
     SVNListParentPath On
     SVNAutoVersioning On
     # We want crypted access
     SSLRequireSSL
     AuthType            basic
     AuthName            "Subversion User Authentication"
     AuthBasicAuthoritative Off
     # Authentication is done by gforge using pam
     AuthPAM_Enabled On
     Require valid-user
     # Following is needed for SVN to function correctly
     ErrorDocument 404 default
     php_flag    engine off
 </Location>

Now, when you restart apache
 /etc/init.d/apache2 restart
everything shoud be working fine...

==== HTTP authentication login via PAM ====
If you want to protect your sites (evolvis main page, wiki, mailman-lists) avoiding any access you can create a simple Basic-Auth-Rule.
Install libapache2-mod-auth-pam and add in ''/etc/apache2/conf.d/gforge.httpd.conf'' at the top

 <Directory />
   ## PAM Auth
 
   AuthType           basic
   AuthName           "Login"
   AuthPAM_Enabled on
   AuthBasicAuthoritative off
   Require valid-user
 
   Options FollowSymLinks
   AllowOverride None
 </Directory>

Now you can login with your evolvis-login.


If not, and ''/var/log/auth.log'' print something like
 Feb  3 10:34:15 evolvis PAM_pgsql[1491]: the database, table and user_column options are required.
 Feb  3 10:34:15 evolvis apache2[1491]: (pam_unix) auth could not identify password for [admin]
you have to make ''/etc/pam_pgsql.conf'' readable for apache
 chmod 644 /etc/pam_pgsql.conf

== SCM (subversion) ==
The svn access should work out of the box. But to use this (short) syntax
 svn checkout svn+ssh://user@svn.yourhost.com/svnroot/'''reponame'''
instead of
 svn checkout svn+ssh://user@svn.yourhost.com/var/lib/gforge/chroot/svnroot/'''reponame'''
you have to set a soft link in the root-directory:
 ln -s /var/lib/gforge/chroot/svnroot/ /svnroot

To use public-key-authentication with svn, instead of passwords, see [[PublicKeySVN]].

== commit-Mail notifications ==
You have to do some configuration to get the commit-mail script working.

check mailman default-conf in
 vim /etc/mailman/mm_cfg.py
and
 vim /usr/lib/mailman/Mailman/Defaults.py
for https URLs, if you enabled default https-forwarding on the machine.

Check for existing mailinglists that falsly use http for default url:
 #! /bin/sh
 for list in `list_lists --bare`
 do echo ----------------------------------
 echo list: $list
 /usr/lib/mailman//bin/dumpdb /var/lib/mailman/lists/$list/config.pck |
 grep "http://"
 echo -----------------------------------
 done

Alter the wrong http-url to https:
 withlist -i LISTNAME

 m.Lock()
 m.web_page_url='https://URL.TO.MAILMAN'
 m.Save()
 Strg+D
	
Backup /etc/postfix/main.cf and edit
 vim /etc/postfix/main.cf 
explained here:
[http://forum.soft32.com/linux2/Bug-424697-gforge-mta-postfix-Postfix-pgsql-settings-ftopict94108.html]

Check /etc/hosts for all needed entries:
 vim /etc/hosts

for example in dev.tarent.de it shoud be
 127.0.0.1 localhost.localdomain localhost
 127.0.1.1 lists.dev.tarent.de dev.tarent.de users.dev.tarent.de lists.dev users.dev dev

Check if dns is resolving all needed domains.

Check, if the host in
 vim /usr/lib/gforge/plugins/scmsvn/lib/post-commit.evolvis
is set right.

Maybe you have to correct it in exisiting projects
 vim /svnroot/PROJECT/hooks/post-commit

Check the rights of /dev/null and /var/lib/gforge/chroot/dev/null
It should be:
 crw-rw-rw- 1 root root 1, 3 2008-11-11 09:15 /dev/null

Check the gforge_mta password in
 vim /etc/postfix/main.cf
If the password is not set, connect to the gforge-database:
 ALTER USER gforge_mta WITH PASSWORD 'PASSWORD';


== Cron jobs ==
[[Cronjob|Here]] you can find a summary of each Cronjob with time specification.

== Links ==
http://php.net/manual/de/migration5.php
http://www.php.net/manual/de/migration51.php
http://www.php.net/manual/de/migration52.php
http://blog.tcg.com/tcg/2008/04/gforge-4511-wor.html
http://ingenico.com.br/download/cd-gforge/php5/
