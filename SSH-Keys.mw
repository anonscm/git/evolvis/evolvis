= SSH-Keys erzeugen und einrichten =

<css>
 pre {
   background-color:#000000;
   color:#CCCCCC;
   font-family:Fixed,monospace;
   font-size:112%;
 }
</css>

Im folgenden wird anhand von Beispielen aufgezeigt, wie man ein RSA-Schlüsselpaar (privat/öffentlich) für die Benutzung mit SSH/SFTP, insbesondere Evolvis, erzeugt.

[[Category:Handbuch]]

----

== Schlüsselpaar erzeugen ==

=== OpenSSH (Unix, Cygwin, GNU) ===

[http://openssh.com/ OpenSSH] ist der gängige Goldstandard unter den SSH-Implementationen und völlig frei. Es ist auf unixartigen Systemen, inklusive mehreren Unix-für-Windows-Frameworks (Cygwin, Interix/SFU/SUA), verfügbar.

Im folgenden wird davon ausgegangen, daß die OpenSSH-Software bereits auf dem Betriebssystem installiert ist. Die Befehle werden vom regulären User in einem Terminal aus dem System abgesetzt, von dem aus der Key benutzt werden soll. Wir erstellen hier einen RSA Version 2 Typ (-t rsa) Schlüssel mit 2048 Bits Größe.

 tglase@meinpc:~ $ <span style="color:#FF0000;">ssh-keygen -t rsa -b 2048</span>
 Generating public/private rsa key pair.
 Enter file in which to save the key (/home/tglase/.ssh/id_rsa): <span style="color:#00FF00;">Enter drücken</span>
 Enter passphrase (empty for no passphrase): <span style="color:#00FF00;">Wunschpaßwort eingeben</span>
 Enter same passphrase again: <span style="color:#00FF00;">Paßwort nochmals eingeben</span>
 Your identification has been saved in /home/tglase/.ssh/id_rsa.
 Your public key has been saved in /home/tglase/.ssh/id_rsa.pub.
 The key fingerprint is:
 9f:4d:8e:c1:4d:15:93:93:6b:8a:ff:a7:28:10:18:02 tglase@meinpc.tarent.de

Der erzeugte Schlüssel wird standardmäßig in '''~/.ssh/id_rsa''' (geheimer Teil) und '''~/.ssh/id_rsa.pub''' (öffentlicher Teil) gespeichert.

==== geheimer Teil ====

Die Datei '''~/.ssh/id_rsa''' auf „meinpc“ ist geheimzuhalten, zu schützen (chmod 0600), und auf jeden Computer, von dem aus der SSH-Zugriff geschehen soll, zu kopieren.

==== öffentlicher Teil ====

Der Inhalt der Datei '''~/.ssh/id_rsa.pub''' (eine ''sehr'' lange Zeile – darauf achten, nicht versehentlich Zeilenumbrüche einzufügen!) wird, wie unten beschrieben, an Evolvis übergeben. Falls Zugriff auf weitere Maschinen nötig ist, sollte diese Datei (als Attachment/Dateianhang) an die Systemadministratoren per eMail versendet werden.

Bitte darauf achten, auch wirklich den öffentlichen Teil zu versenden – die Datei endet auf '''.pub''' und besteht nur aus einer, ''sehr'' langen, Zeile.

----

=== PuTTY (Windows®, NT®, Windows CE®, ggf. Linux) ===

[http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html PuTTY] ist eine alternative SSH-Implementierung, die sich auf Windows® und NT® sowie dessen Mobilversionen größter Beliebtheit erfreut, jedoch auch auf Linux portiert wurde.

Zur Schlüsselerstellung wird der graphische PuTTYgen benötigt; um den Schlüssel auf Sitzungen zu binden, wird das graphische Hauptprogramm PuTTY benutzt. Dateiübertragungen können hiernach mit dem kommandozeilenbasierten PSFTP ausgeführt werden.

==== PuTTYgen ====

Zunächst muß PuTTYgen gestartet werden. Es präsentiert sich uns wie folgt:

[[File:01-puttygen-rsa2-2048-generate.png]]

Der Schlüsseltyp (RSA Version 2) ist bereits korrekt eingestellt, allerdings noch mit der falschen Größe von 1024 Bit, die wir in 2048 ändern, bevor wir „Generate“ anklicken.

[[File:02-puttygen-working.png]]

Zur Schlüsselerzeugung benötigt der Generator Zufall, welchen er aus Mausbewegungen über sein Programmfenster zu extrahieren versucht (eine Alternative ist, den Schlüssel mit OpenSSH zu erzeugen und in PuTTYgen nur zu importieren, das ginge über das „Conversions“-Menü). Hier schubsen wir also die Maus möglichst zufällig hin und her.

[[File:03-puttygen-results.png]]

Der Schlüssel wurde fertig erzeugt; uns wird bereits der öffentliche Teil im OpenSSH-Format, das [https://evolvis.org/projects/evolvis/ Evolvis] erwartet, oben angezeigt; diesen kopieren wir:

[[File:04-puttygen-public.png]]

Dann fügen wir die Zwischenablage, wie unten beschrieben, ins Evolvis ein.

[[File:03-puttygen-results.png]]

Außerdem müssen wir noch ein Paßwort für den geheimen Schlüssel festlegen und mindestens ihn (ggf. aber auch den ''public key'' im PuTTY-Format) auf der lokalen Festplatte speichern, um ihn später benutzen zu können.

[[File:05-puttygen-private.png]]

Danach kann PuTTYgen beendet werden.

==== PuTTY Session ====

Jetzt können wir in PuTTY eine „Session“ einrichten, z.B. mit der passenden Server-IP und dem Benutzernamen, aber konkret auch dem SSH-Key – diese Option befindet sich hier:

[[File:06-putty-session-configuration.png]]

Durch Klicken auf „Browse…“ öffnet sich ein Dateiauswahldialog, in dem der im vorigen Schritt gespeicherte private Schlüssel ausgewählt werden kann:

[[File:07-putty-load-ppk-file.png]]

Als weiterführende Anleitung zum Konfigurieren einer Session wird auf die [http://the.earth.li/~sgtatham/putty/0.61/htmldoc/Chapter4.html#config-session PuTTY-Dokumentation] verwiesen.

==== PSFTP ====

[[File:08-psftp-help.png]]

Zum Benutzen von PSFTP kann man entweder vermittels „-load sessionname“ eine im vorigen Schritt innerhalb der PuTTY-GUI konfigurierte Session auswählen (rot) oder mit „-i xx.ppk user@host.com“ (blau) direkt den Key und Zielserver angeben. (In Violett wird aufgezeigt, wie die Hilfe des Kommandozeilenprogramms '''psftp''' angezeigt werden kann.)

----

== Öffentlichen Schlüssel in Evolvis einpflegen ==

Als Voraussetzung wird von einem existierenden Account im Evolvis-System ausgegangen; bitte zunächst einloggen.

[[File:09-evolvis-main-goto-account.png]]

Hier wird die „Mein Account“-Seite aufgerufen, auf welcher bitte ganz nach unten gescrollt wird:

[[File:10-evolvis-account-editkeys.png]]

Im ersten Schritt wird hier lediglich auf „Edit Keys“ geklickt. Dies öffnet folgendes Formular:

[[File:11-evolvis-account-editkeys-form.png]]

In das große Eingabefeld wird nun der öffentliche Teil des RSA-Schlüsselpaars eingefügt, danach auf „Update“ geklickt. Wir kommen zurück in die Accountkonfiguration:

[[File:10-evolvis-account-editkeys.png]]

Nunmehr sollte, als dritter Schritt, überprüft werden, daß die korrekte Anzahl Schlüssel an der rot markierten Stelle angezeigt wird – hier 2 (zwei), beim ersten Durchlaufen dieser Anleitung wäre dies vermutlich eine 1 (eins). Wenn die Zahl ''nicht'' stimmt, wurde im vorigen Schritt falsch (mit Zeilenumbruch?) eingefügt, dann bitte nochmal versuchen, ggf. mit einem anderen Webbrowser.

=== Evolvis verteilt den Schlüssel ===

Spätestens eine Stunde hiernach (die Aktualisierung wird standardmäßig immerhin jede runde Viertelstunde durchgeführt) kann nun der Schlüssel zum Einloggen benutzt werden, entweder über die PuTTY-GUI oder via Kommandozeile:

 tglase@meinpc:~ $ <span style="color:#FF0000;">ssh mirabilos@evolvis.org</span>
 Linux www 2.6.26-2-xen-686 #1 SMP Thu Jan 27 05:44:37 UTC 2011 i686
 Last login: Sat Mar 19 17:42:41 2011 from …
 ( success ( 2 2 ( ) ( edit-pipeline svndiff1 absent-entries commit-revprops
 depth log-revprops partial-replay ) ) ) <span style="color:#FF0000;">^C</span>Connection to evolvis.org closed.

Die „geklammerte“ Ausgabe ist ein Zeichen dafür, daß Zugang über die ''anonsvnsh'', welche einem Benutzer lediglich SVN (Subversion) und SFTP (Sicherer Dateitransfer) erlaubt, funktioniert. Gegebenenfalls muß der Schlüssel angegeben werden:

 tglase@meinpc:~ $ <span style="color:#FF0000;">ssh -i ~/.ssh/id_rsa mirabilos@evolvis.org</span>
 Enter passphrase for key '/home/tglase/.ssh/id_rsa': <span style="color:#00FF00;">Paßwort hier eingeben</span>
 Linux www 2.6.26-2-xen-686 #1 SMP Thu Jan 27 05:44:37 UTC 2011 i686
 Last login: Tue Aug  9 15:37:28 2011 from …
 ( success ( 2 2 ( ) ( edit-pipeline svndiff1 absent-entries commit-revprops
 depth log-revprops partial-replay ) ) ) <span style="color:#FF0000;">^C</span>Connection to evolvis.org closed.

Das Benutzen der Kommandozeilenversion von PuTTY, '''plink''', gestaltet sich gleich:

 I:\><span style="color:#FF0000;">plink -i xx.ppk mirabilos@evolvis.org</span>
 The server's host key is not cached in the registry. You have no
 guarantee that the server is the computer you think it is.
 The server's rsa2 key fingerprint is:
 ssh-rsa 2048 b6:06:a7:b1:1c:43:db:6f:15:29:d8:49:7a:30:3d:a1
 If you trust this host, enter "y" to add the key to PuTTY's cache
 and carry on connecting.
 If you want to carry on connecting just once, without adding the
 key to the cache, enter "n".
 If you do not trust this host, press Return to abandon the connection.
 Store key in cache? (y/n) <span style="color:#FF0000;">y</span>
 Using username "mirabilos".
 Passphrase for key "imported-openssh-key": <span style="color:#00FF00;">Paßwort hier eingeben</span>
 Linux www 2.6.26-2-xen-686 #1 SMP Thu Jan 27 05:44:37 UTC 2011 i686
 Last login: Tue Aug  9 15:39:05 2011 from 
 ( success ( 2 2 ( ) ( edit-pipeline svndiff1 absent-entries commit-revprops
 depth log-revprops partial-replay ) ) )<span style="color:#FF0000;">^C</span>
 I:\><span style="color:#00FF00;">_</span>

Der [https://www.mirbsd.org/htman/i386/man1/ssh-agent.htm ssh-agent(1)] ist ein hilfreiches Tool, durch welchen man sich die dauernde Eingabe des Paßwortes bei geeigneter Konfiguration sparen kann. PuTTY bietet mit [http://the.earth.li/~sgtatham/putty/0.61/htmldoc/Chapter9.html Pageant] ein Äquivalent.

== Weiterführende Lektüre ==

* [http://www.onsight.com/faq/ssh/ssh-faq.html The Secure Shell FAQ] – Frequently Asked Questions
* [http://the.earth.li/~sgtatham/putty/0.61/htmldoc/AppendixA.html PuTTY FAQ] (und das Handbuch, generell)
* [http://docstore.mik.ua/orelly/networking_2ndEd/ssh/ch01_01.htm Introduction to SSH (SSH, The Secure Shell: The Definitive Guide)], O’Reilly
* [http://www.math.osu.edu/support/ssh/ SSH Introduction │ Department of Mathematics], Ohio State University
